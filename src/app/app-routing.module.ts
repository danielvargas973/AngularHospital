import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { PatientsComponent } from './patients/patients.component';
import { NewComponent } from './patients/new/new.component';
import { ListComponent } from './patients/list/list.component';


const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'patient', component: PatientsComponent, children: [
    {path: 'new', component: NewComponent},
    {path: 'list', component: ListComponent}
  ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
