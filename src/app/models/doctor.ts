export interface Doctor {
    id: number;
    fullName: string;
    specialty: string;
    credentialNumber: number;
    nameHospital: string;
  }
