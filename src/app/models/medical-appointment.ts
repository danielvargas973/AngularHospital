export interface MedicalAppointment{
    id: number;
    doctorId: number;
    patientId: number;
    officeNumber: number;
    reasonQuote: string;
}
