export interface Patient{
    Id: number;
    FullName: string;
    SocialSecurityName: number;
    PostCode: number;
    TelephoneContact: number;
}
