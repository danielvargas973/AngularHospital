import { Component, OnInit } from '@angular/core';
import { Patient } from 'src/app/models/patient';
import { PatientService } from '../../services/patientService';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
})
export class ListComponent implements OnInit {
  patientCollection: Patient[] = [];
  constructor(private router: Router, private patientService: PatientService) {
    this.getAllPatients();
  }

  ngOnInit(): void {}
  getAllPatients() {
    this.patientService.GetAllPatients().subscribe(
      (patient) => {
        this.patientCollection = patient;
        console.log(patient.patient);
      },
      (error) => {
        console.log(JSON.stringify(error));
      }
    );
  }
  redirectToPage(urlPage: any) {
    this.router.navigate([urlPage]);
  }
  deletePatient(id: number) {
    const option = confirm('¿Está seguro que desea eliminar este paciente?');
    if (option) {
      this.patientService.DeletePatient(id).subscribe(
        (patient) => {
          this.getAllPatients();
        },
        (error) => {
          console.log(JSON.stringify(error));
        }
      );
    }
  }
}
