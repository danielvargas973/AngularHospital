import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Patient } from '../../models/patient';
import { Router } from '@angular/router';
import { PatientService } from '../../services/patientService';
declare var $: any;

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.css'],
})
export class NewComponent implements OnInit {
  constructor(private router: Router, private patientService: PatientService) {}

  patient: Patient = {
    Id: 0,
    FullName: null,
    SocialSecurityName: 0,
    PostCode: 0,
    TelephoneContact: 0,
  };
  validation: string;
  ngOnInit(): void {}
  newPatient(myForm: NgForm) {
    if (myForm.valid === true) {
      this.registerPatient();
    } else {
      alert('Todos los campos son obligatorios');
    }
  }
  registerPatient() {
    return this.patientService.RegisterPatient(this.patient).subscribe(
      (patient) => {
        this.redirectToPage('/patient/list');
      },
      (error) => {
        console.log(JSON.stringify(error));
      }
    );
  }
  redirectToPage(urlPage: any) {
    this.router.navigate([urlPage]);
  }
}
