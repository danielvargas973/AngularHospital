import { Injectable } from '@angular/core';
import { Patient } from '../models/patient';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class PatientService {
  private path = 'https://localhost:44355/api/patient/';
  constructor(private httpClient: HttpClient) {}
  GetAllPatients(): Observable<any> {
    return this.httpClient.get<Patient[]>(this.path);
  }
  RegisterPatient(patient: Patient) {
    const json = JSON.stringify(patient);
    const header = new HttpHeaders().set('content-type', 'application/json');
    return this.httpClient.post(this.path, json, { headers: header });
  }
  DeletePatient(id: number): Observable<any> {
    return this.httpClient.delete(this.path + id);
  }
}
